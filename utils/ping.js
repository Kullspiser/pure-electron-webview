const Store = require('electron-store');
const interval = require('interval-promise');
const axios = require('axios');
const {ECHO_ROUTE} = require("../auth.service");

const store = new Store();

const startPing = (successCallback, errorCallback) => {
    const url = store.get('APP_URL') + ECHO_ROUTE;

    let errorCount = 0;
    let successCount = 0;

    interval(async (_, stop) => {
        if (successCount >= 3) {
            successCallback();
            stop();
        }
        try {
            await axios.get(url);
            successCount++;
        } catch (e) {
            errorCount++;
            if (errorCount >= 5) {
                if (errorCallback) {
                    errorCallback(e);
                }
                stop();
            }
        }
    }, 1000);
};

module.exports = {
    startPing,
};
