const dialogs = require('dialogs');
const Store = require('electron-store');
const { startPing } = require('./utils/ping');
const {ACTIVATE_ROUTE } = require('./auth.service');
const axios = require("axios");

const store = new Store();

window.onload = () => {
    const d = dialogs();

    if (store.get('APP_URL') === undefined) {
        d.prompt('Пожалуйста, предоставьте адрес вашего приложения', 'https://google.com', data => {
            if (data === undefined) {
                window.location.reload();
            } else {
                store.set('APP_URL', data);
                window.location.reload();
            }
        });
    } else {
        startPing(() => {
            const webView = document.getElementById('primary_webview');
            webView.setAttribute('src', store.get('APP_URL'));
        },(e) => {
            console.error(e);
            if (e.response.status === 404) {
                d.prompt('Адрес приложения указан неверно или недоступен в данный момент. Укажите другой адрес', store.get('APP_URL'), data => {
                    if (data === undefined) {
                        window.location.reload();
                    } else {
                        store.set('APP_URL', data);
                        window.location.reload();
                    }
                });
            }
            if (e.response.status === 401) {
                d.prompt('Вы не авторизованы. Укажите свой ключ', '', data => {
                    if (data === undefined) {
                        window.location.reload();
                    } else {
                        axios.post(store.get('APP_URL') + ACTIVATE_ROUTE(data)).then(() => {
                            window.location.reload();
                        }).catch(err => {
                            if (err.response && err.response.status === 403) {
                               d.alert('Введен неверный код или ваш IP-адрес не является локальным.');
                            }
                            window.location.reload();
                        });
                    }
                });
            }
        });
    }
}
